// Package linuxpath implements utility for manipulating filename paths in a way linux bash does.
package linuxpath

import (
	"os/user"
	"path"
	"path/filepath"
	"strings"
)

// ExpendHome returns the path which the tilde-prefix will be replaced to correct user home directory
func ExpendHome(p string) (string, error) {
	if !strings.HasPrefix(p, "~/") {
		return p, nil
	}

	paths := strings.Split(p, "/")
	usr, err := user.Current()
	if err != nil {
		return "", err
	}

	paths[0] = usr.HomeDir

	return filepath.Join(paths...), nil
}

// Basename returns the filename withoiut extension
func Basename(p string) string {
	fileName := path.Base(p)
	ext := path.Ext(fileName)
	if fileName == ext {
		return fileName
	}
	return fileName[:len(fileName)-len(ext)]
}

// Extname returns the extension name of file
func Extname(p string) string {
	basename := Basename(p)
	ext := path.Ext(p)

	if len(ext) == 0 {
		return ""
	}

	if basename == ext {
		return ""
	}
	return ext[1:]
}

// SplitFilename returns pair of file basename and extension name
func SplitFilename(f string) (string, string) {
	return Basename(f), Extname(f)
}

// Split returns aboslute path after expend home direcotry, file base name and extension
func Split(path string) (dir, basename, extname string, err error) {
	p, err := ExpendHome(path)

	if err != nil {
		return
	}

	dir, f := filepath.Split(p)

	dirLen := len(dir)
	if (dirLen > 1) && strings.HasSuffix(dir, "/") {
		dir = dir[:dirLen-1]
	}
	basename = Basename(f)
	extname = Extname(f)

	return
}
