package linuxpath_test

import (
	"log"
	"os/user"
	"path/filepath"
	"testing"

	"bitbucket.org/ezchi/linuxpath"
)

type stringPair struct {
	orig   string
	expect string
}

var homeDir string

func init() {
	usr, _ := user.Current()
	homeDir = usr.HomeDir
}

func TestExpendHome(t *testing.T) {
	testPair := []stringPair{
		{"../bin", "../bin"},
		{"~/bin", filepath.Join(homeDir, "bin")},
	}

	for _, pair := range testPair {
		p, err := linuxpath.ExpendHome(pair.orig)
		if err != nil {
			log.Fatal(err)
		}
		if p != pair.expect {
			t.Error(
				"\nFor     :", pair.orig,
				"\nexpected:", pair.expect,
				"\ngot     :", p,
			)
		}
	}
}

func TestBasename(t *testing.T) {
	testPair := []stringPair{
		{"/a/b/c", "c"},
		{"/a/b/c.txt", "c"},
		{"~/a/b/c.d", "c"},
		{"~/a/b/.c", ".c"},
	}

	for _, pair := range testPair {
		b := linuxpath.Basename(pair.orig)
		if b != pair.expect {
			t.Error(
				"\nFor     :", pair.orig,
				"\nexpected:", pair.expect,
				"\ngot     :", b,
			)
		}
	}
}

func TestExtname(t *testing.T) {
	testPair := []stringPair{
		{"/a/b/c", ""},
		{"/a/b/c.txt", "txt"},
		{"~/a/b/c.d", "d"},
		{"~/a/b/.c", ""},
	}

	for _, pair := range testPair {
		b := linuxpath.Extname(pair.orig)
		if b != pair.expect {
			t.Error(
				"\nFor     :", pair.orig,
				"\nexpected:", pair.expect,
				"\ngot     :", b,
			)
		}
	}
}

func TestSplitFilename(t *testing.T) {
	tests := [][]string{
		{"/a/b/c", "c", ""},
		{"/a/b/c.txt", "c", "txt"},
		{"~/a/b/c.d", "c", "d"},
		{"~/a/b/.c", ".c", ""},
	}

	for _, test := range tests {
		b, e := linuxpath.SplitFilename(test[0])
		if b != test[1] || e != test[2] {
			t.Error(
				"\nFor                    :", test[0],
				"\nexpected filename      :", test[1],
				"\ngot filename           :", b,
				"\nexpected extension name:", test[2],
				"\ngot extension name     :", e,
			)
		}
	}
}

func TestSplit(t *testing.T) {
	tests := [][]string{
		{"/a/b/c", "/a/b", "c", ""},
		{"/a/b/c.txt", "/a/b", "c", "txt"},
		{"~/a/b/c.d", filepath.Join(homeDir, "a/b"), "c", "d"},
		{"~/a/b/.c", filepath.Join(homeDir, "a/b"), ".c", ""},
	}

	for _, test := range tests {
		d, b, e, _ := linuxpath.Split(test[0])
		if d != test[1] || b != test[2] || e != test[3] {
			t.Error(
				"\nFor                    :", test[0],
				"\nExpected dir           :", test[1],
				"\nGot                    :", d,
				"\nExpected filename      :", test[2],
				"\nGot filename           :", b,
				"\nExpected extension name:", test[3],
				"\nGot extension name     :", e,
			)
		}
	}
}
